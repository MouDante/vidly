namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsersAgain : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5d7f8cf0-6052-4f13-aa05-b9c4b7b02408', N'admin@vidly.com', 0, N'ADvp6C5YFX1zBWBrKgJWEsc1SfeCSLCE5tc6I0zOllIaMRezzWjLv+Xj7hcpBkoFHg==', N'658f0b90-a52e-4164-a5d5-bb9561be1aaf', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a2b451c9-f09a-4f17-bbf3-dcf1905df703', N'guest@vidly.com', 0, N'ACvIWEphEu4BuMFfMLF/ooFHxLSjnOzwq0GE4EZS9x7lbHCAFndzaf7IITa7HGyYjA==', N'e9713ba4-b99e-49b9-822b-3d38675c699d', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'56055736-55c4-41cb-9d82-261a50b41f61', N'CanManageMovie')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5d7f8cf0-6052-4f13-aa05-b9c4b7b02408', N'56055736-55c4-41cb-9d82-261a50b41f61')

");
        }
        
        public override void Down()
        {
        }
    }
}
